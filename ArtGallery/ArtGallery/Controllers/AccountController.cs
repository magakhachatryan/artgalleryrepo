﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ArtGal.Controllers
{
    [ApiController]
    [Route("api/account")]
    public class AccountController : ControllerBase
    {

        private IUserService userService;
       
        //  ALLOWS US TO AUTENTICATE USER
        
        //  private ApplicContext _dbcontext;

        // private SignInManager<User> _signInManager; //  for coocie

        public AccountController( IUserService userservice)//SignInManager<User> signInManager)
        {
           
            this.userService = userservice;
            // _signInManager = signInManager;
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]UserDTO user)
        {
            try
            { var data = await userService.Add(user);
                return Ok(data);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([FromBody]LoginDTO user)
        {
            try
            {
                var data = await userService.SignIn(user);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {


            try
            {
                var data = await userService.Logout();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
