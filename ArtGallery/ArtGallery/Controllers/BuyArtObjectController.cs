﻿using System;
using System.Threading.Tasks;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryBLL.ServicesBLL;
using Microsoft.AspNetCore.Mvc;

namespace ArtGallery.Controllers
{
    [Route("api/buyArtObject")]
    [ApiController]
    public class BuyArtObjectController : ControllerBase
    {
        private IArtObjectService artObjectService;
        public BuyArtObjectController(IArtObjectService artObjectService)
        {
            this.artObjectService = artObjectService;
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await artObjectService.BuyObject(id);
                return Ok("Object is sold");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

    }
}
