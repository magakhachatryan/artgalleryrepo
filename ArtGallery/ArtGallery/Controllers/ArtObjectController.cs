﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDTO.EntitiesDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ArtGallery.Controllers
{
    [Route("api/artobject")]
    [ApiController]
    public class ArtObjectController : ControllerBase
    {
        private IArtObjectService artObjectService;
        public ArtObjectController(IArtObjectService artObjectService)
        {
            this.artObjectService = artObjectService;
        }

        // GET: api/artobject
        //[Route("~/api/Get")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var data = await artObjectService.GetAll();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        // GET: api/artobject/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {

            try
            {
                var data = await artObjectService.GetById(id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

    
        // POST: api/artobject
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ArtObjectDTO artObject)
        {
            try
            {
                var data = await artObjectService.Add(artObject);
                return Ok("Posted!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // PUT: api/artobject/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ArtObjectDTO artObject)
        {
            if (id == artObject.Index)
            {
                artObjectService.Update(artObject);
                return Ok("Put!");
            }
            return BadRequest(id);
        }

        // DELETE: api/artobject/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var data = await artObjectService.GetById(id);
            if (data == null)
            {
                return BadRequest(id);
            }
            else
            {
                await artObjectService.Remove(id);
                return Ok("Successfully deleted!");
            }

        }
    }
}
