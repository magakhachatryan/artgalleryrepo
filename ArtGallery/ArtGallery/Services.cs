﻿using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryBLL.ServicesBLL;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArtGallery
{
    static public class Services
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IArtObjectService, ArtObjectService>();
           // services.AddScoped<IShoppingCartService, ShoppingCartService>();
            services.AddScoped<IUserService, UserService>();
            return services;
        }
    }
}
