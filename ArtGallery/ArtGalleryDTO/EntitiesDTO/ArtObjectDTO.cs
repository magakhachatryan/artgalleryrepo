﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ArtGalleryDTO.InterfacesDTO;
using Enums;

namespace ArtGalleryDTO.EntitiesDTO
{
    public class ArtObjectDTO :IBaseEntityDTO
    {
        public int Index { get; set; }
        public string name { get; set; }
        public string artist { get; set; }
        public Types type { get; set; }
        public string description { get; set; }
        public DateTime time { get; set; }
        public decimal price { get; set; }
        public string sellerId { get; set; }

       // public ICollection<ShoppingCartDTO> shoppingCart { get; set; }
    }
}
