﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using ArtGalleryDTO.InterfacesDTO;

namespace ArtGalleryDTO.EntitiesDTO
{
    
        public class UserDTO:IBaseEntityDTO
        {
            [Required]
            public string Address { get; set; }
            [Required]
            public string Account { get; set; }
            [Required]
            public string PhoneNumber { get; set; }
            [Required]
            public string Email { get; set; }
            [Required]
            public string UserName { get; set; }
            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }
            [Required]
            [Compare("Password", ErrorMessage = "Incorrect PAssword")]
            [DataType(DataType.Password)]
            public string PasswordConfirm { get; set; }
            public int Index { get ; set ; }
    }
        //public ICollection<ArtObjectDTO> artObjects { get; set; }
        //public ICollection<ShoppingCartDTO> shoppingCart { get; set; }
    
}
