﻿using Microsoft.EntityFrameworkCore;
using ArtGalleryDAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace ArtGalleryDAL
{
    public class ArtGalleryContext : IdentityDbContext<User>// DbContext
    {

        public ArtGalleryContext(DbContextOptions<ArtGalleryContext> options) : base(options)
        {

           // Database.Migrate();
        }

      
       
       // public DbSet<ShoppingCart> ShoppingCarts { get; set; }
        public DbSet<ArtObject> ArtObjects { get; set; }
        public DbSet<User> User { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<User>(b =>
            {
                b.HasKey(u => u.Id);
                b.HasIndex(u => u.UserName).IsUnique();
                b.HasIndex(u => u.NormalizedUserName).IsUnique();
                b.HasIndex(u => u.Email).IsUnique();
                b.HasIndex(u => u.NormalizedEmail).IsUnique();
                b.HasIndex(u => u.PhoneNumber).IsUnique();
                b.ToTable("User");
                b.Property(u => u.ConcurrencyStamp).IsConcurrencyToken();


                b.Property(u => u.UserName).HasMaxLength(256);
                b.Property(u => u.NormalizedUserName).HasMaxLength(256);
                b.Property(u => u.Email).HasMaxLength(256);
                b.Property(u => u.NormalizedEmail).HasMaxLength(256);

               
            });
            modelBuilder.Entity<IdentityUserLogin<string>>().HasNoKey();
            modelBuilder.Entity<IdentityUserRole<string>>().HasNoKey();
            modelBuilder.Entity<IdentityUserToken<string>>().HasNoKey();

            modelBuilder.Entity<ArtObject>()
                .HasKey(p => p.Index);
            modelBuilder.Entity<ArtObject>()
                .Property(f => f.Index)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<ArtObject>()
               .HasOne(t => t.seller)
               .WithMany(p => p.artObjects)
               .HasForeignKey(p => p.sellerId)
               .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ArtObject>()
                .Property(p => p.sellerId);

            //modelBuilder.Entity<ShoppingCart>()
            //   .HasKey(p => p.Index);
            //modelBuilder.Entity<ShoppingCart>()
            //   .Property(f => f.Index)
            //   .ValueGeneratedOnAdd();
            //modelBuilder.Entity<ShoppingCart>()
            //   .HasOne(p => p.artObject)
            //   .WithMany(p => p.shoppingCart)
            //   .HasForeignKey(p => p.artObjectId)
            //   .OnDelete(DeleteBehavior.Cascade);
            //modelBuilder.Entity<ShoppingCart>()
            //   .HasOne(p => p.user)
            //   .WithMany(p => p.shoppingCart)
            //   .HasForeignKey(p => p.userId)
            //   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}