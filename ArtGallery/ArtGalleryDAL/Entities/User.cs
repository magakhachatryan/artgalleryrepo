﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using System.Text;
using ArtGalleryDAL.Interfaces;
using System.ComponentModel.DataAnnotations.Schema;

namespace ArtGalleryDAL.Entities
{
    public class User: IdentityUser,IBaseEntity
    {
        public string Account { get; set; }
        public string Address { get; set; }
        public ICollection<ArtObject> artObjects { get; set; }
       // public ICollection<ShoppingCart> shoppingCart { get; set; }

        [NotMapped]
        public int Index{ get; set; }
    }
}
