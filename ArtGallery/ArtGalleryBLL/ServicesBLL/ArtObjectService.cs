﻿using System;
using System.Collections.Generic;
using System.Text;
using ArtGalleryDTO.EntitiesDTO;
using ArtGalleryDAL.Entities;
using ArtGalleryBLL.InterfacesBLL;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using ArtGalleryDAL;
using Microsoft.EntityFrameworkCore;

namespace ArtGalleryBLL.ServicesBLL
{
    public class ArtObjectService :ServiceBase<ArtObject, ArtObjectDTO>, IArtObjectService
    {
        public ArtObjectService(IMapper  mapper, ArtGalleryContext context) : base(mapper,context)
        {


        }

        public Task<int> BuyAll()
        {
            throw new NotImplementedException();
        }

       
        public async Task<ArtObjectDTO> BuyObject(int objId)
        {
            ArtObjectDTO result = null;
            if (await this.GetById(objId) != null)
                result = await Remove(objId);
            return result;
        }
    }
}
