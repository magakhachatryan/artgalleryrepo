﻿using System;
using System.Collections.Generic;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDAL.Interfaces;
using ArtGalleryDAL;
using ArtGalleryDTO.InterfacesDTO;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Linq;
using Microsoft.AspNetCore.Identity;

namespace ArtGalleryBLL.ServicesBLL

{
    public class ServiceBase<TEntity, TDto> : IBaseService<TEntity, TDto>
    where TEntity : class, IBaseEntity
    where TDto : class, IBaseEntityDTO
    {
        protected readonly ArtGalleryContext dbContext;
        protected readonly DbSet<TEntity> entities;
        protected readonly IMapper mapper;

        public ServiceBase(IMapper mapper, ArtGalleryContext dbContext)
        {
            this.dbContext = dbContext;
            this.entities = this.dbContext.Set<TEntity>();
            this.mapper = mapper;
        }
        public async Task<TDto> Add(TDto model)
        {
            var toAdd = mapper.Map<TEntity>(model);
            var added = await entities.AddAsync(toAdd);
            await dbContext.SaveChangesAsync();
            var addedentity = added.Entity;
            var addedDto = mapper.Map<TDto>(addedentity);
            return addedDto;

        }

        public async Task<List<TDto>> Get(Expression<Func<TEntity, bool>> expression)
        {
            var querylist = entities.Where(expression);
            var list = await querylist.ToListAsync();
            var listDto = mapper.Map<List<TDto>>(list);
            return listDto;

        }

        public async Task<List<TDto>> GetAll()
        {
            List<TEntity> list = await entities.ToListAsync();
            var listDto = mapper.Map<List<TDto>>(list);
            return listDto;

        }

        public async Task<TDto> GetById(int id)
        {
            var entity = await entities.FindAsync(id);
            var entityDto = mapper.Map<TDto>(entity);
            return entityDto;
        }

        public async Task<TDto> Remove(int id)
        {
            var toRemove = await entities.FindAsync(id);
            var removed = entities.Remove(toRemove);
            await dbContext.SaveChangesAsync();
            var removedEntity = removed.Entity;
            var removedDto = mapper.Map<TDto>(removedEntity);
            return removedDto;
        }

        public async Task<TDto> Update(TDto model)
        {
            var newEntity = mapper.Map<TEntity>(model);
            entities.Update(newEntity);
             dbContext.SaveChanges();
            return model;
        }

    }
}
