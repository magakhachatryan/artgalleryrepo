﻿using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDAL;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace ArtGalleryBLL.ServicesBLL
{
    public class UserService : ServiceBase<User, UserDTO>, IUserService
    {
        private UserManager<User> _userManager;
        private SignInManager<User> _signInManager;
        public UserService(UserManager<User> userManager, SignInManager<User> signinManager, IMapper mapper, ArtGalleryContext context) : base(mapper, context)
        {
            this._userManager = userManager;
            this._signInManager = signinManager;
        }

        public new async Task<UserDTO> Add(UserDTO model)
        {
            var toAdd = mapper.Map<User>(model);
            var data = await _userManager.CreateAsync(toAdd, model.Password);

            UserDTO addedDto = null;
            if (data.Succeeded)
            {
                addedDto = model;
            }

            return addedDto;

        }

        public async Task<LoginDTO> SignIn(LoginDTO user)
        {
            var result =
                await _signInManager.PasswordSignInAsync(user.Email, user.Password, user.RememberMe, false);
            LoginDTO signedInDTO = null;
            if (result.Succeeded)
            {
                signedInDTO = user;
            }

            return signedInDTO;

        }

        public async Task<bool> Logout()
        {
            bool signedout = false;
            // удаляем аутентификационные куки
            try
            {
                await _signInManager.SignOutAsync();
                signedout = true;
            }
            catch(Exception ex)
            {
                return (signedout);
            }
            return signedout;
            
        }
    }
}
