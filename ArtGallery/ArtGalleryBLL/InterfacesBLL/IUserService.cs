﻿using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ArtGalleryBLL.InterfacesBLL
{
    public interface IUserService:IBaseService<User,UserDTO>
    {
        public Task<LoginDTO> SignIn(LoginDTO user);
        public Task<bool> Logout();
    }
}
